package main

import (
	"fmt"
	"log"

	"gitlab.com/z.amir/community-administration-cli/helper"
	usecases "gitlab.com/z.amir/community-administration-cli/internal/usecase"

	"go.uber.org/zap"
)

func main() {
	logger, err := newZapLogger()
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	schemaUsecase := usecases.NewSchemaUsecase(logger)

	err = schemaUsecase.GenerateEventSchema()
	if err != nil {
		logger.Fatal("--- schemaUsecase GenerateEventSchema: FAILED ---", zap.Error(err))
	}

	logger.Info("--- schemaUsecase GenerateEventSchema: SUCCEEDED ---")

	typeEventSchemaNameSlice := []string{
		"community",
		"member",
	}

	for _, typeEventSchemaName := range typeEventSchemaNameSlice {
		typeEventSchemaSlice, err := schemaUsecase.ValidateAndUnmarshalTypeEventSchemaYaml(typeEventSchemaName)
		if err != nil {
			logger.Fatal("--- schemaUsecase ValidateAndUnmarshalTypeMemberEventYaml: FAILED ---", zap.Error(err))
		}

		logger.Info("--- schemaUsecase ValidateAndUnmarshalTypeMemberEventYaml: SUCCEEDED ---")

		err = schemaUsecase.GenerateGoCode(typeEventSchemaName, typeEventSchemaSlice)
		if err != nil {
			logger.Fatal("--- schemaUsecase GenerateGoEnum: FAILED ---", zap.Error(err))
		}
	}

	err = schemaUsecase.GenerateCommunitySchema()
	if err != nil {
		logger.Fatal("--- schemaUsecase GenerateCommunitySchema: FAILED ---", zap.Error(err))
	}

	logger.Info("--- schemaUsecase GenerateCommunitySchema: SUCCEEDED ---")

	err = schemaUsecase.GeneratePersonsSchema()
	if err != nil {
		logger.Fatal("--- schemaUsecase GeneratePersonsSchema: FAILED ---", zap.Error(err))
	}

	logger.Info("--- schemaUsecase GeneratePersonsSchema: SUCCEEDED ---")
}

func newZapLogger() (*zap.Logger, error) {
	config := helper.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
