FROM golang:1-alpine AS build

RUN apk add --update --no-cache git

ADD . /go/src/community-administration-cli/
WORKDIR /go/src/community-administration-cli
RUN go mod download
RUN go build -o dist/bin/community-administration-cli .

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/community-administration-cli/dist/bin/community-administration-cli /usr/local/bin/community-administration-cli

RUN apk update

RUN apk add --upgrade texlive
RUN apk add --upgrade pandoc
RUN apk add --update npm

RUN npm install -g prettier

WORKDIR /cli

ENTRYPOINT ["/usr/local/bin/community-administration-cli"]
