package main

import (
	"embed"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/z.amir/community-administration-cli/helper"
	"gitlab.com/z.amir/community-administration-cli/internal/model"
	"gitlab.com/z.amir/community-administration-cli/internal/repositories"
	usecases "gitlab.com/z.amir/community-administration-cli/internal/usecase"

	"github.com/urfave/cli/v2"
	"go.uber.org/zap"
)

//go:embed schema/*.schema.json
var fs embed.FS

func main() {
	logger, err := newZapLogger()
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	administrationDirectoryPath, err := os.Getwd()
	if err != nil {
		log.Fatalf("os.Getwd failed: %v", err)
	}

	storageRepository := repositories.NewStorageFileClient(
		logger,
		administrationDirectoryPath,
	)

	communityEventRepository := repositories.NewCommunityEventYamlClient(
		logger,
		fs,
		filepath.Join(administrationDirectoryPath, "data", "community_events.yaml"),
	)

	memberEventRepository := repositories.NewMemberEventYamlClient(
		logger,
		fs,
		filepath.Join(administrationDirectoryPath, "data", "member_events.yaml"),
	)

	communityUsecase := usecases.NewCommunityUsecase(logger, communityEventRepository, storageRepository)

	personUsecase := usecases.NewPersonUsecase(logger, communityEventRepository, memberEventRepository, storageRepository)

	subscriptionRenewalUsecase := usecases.NewSubscriptionRenewalUsecase(logger, storageRepository)

	app := cli.NewApp()
	app.Name = "Community Administration CLI"
	app.Usage = "A event sourcing cli for community administration"
	app.Version = "v0.0.6"

	app.Before = func(context *cli.Context) error {
		timestamp := context.Timestamp("timestamp")
		if timestamp == nil {
			time := time.Now().UTC()

			timestamp = &time
		}

		context.App.Metadata["timestamp"] = *timestamp

		return nil
	}

	app.Flags = []cli.Flag{&cli.TimestampFlag{
		Name:     "timestamp",
		Usage:    "Go back in time or go to the future with timestamp. Leave empty to use current time.",
		Required: false,
		Layout:   "2006-01-02T15:04:05",
		Timezone: time.Local,
	}}

	app.Action = func(context *cli.Context) error {
		timestamp := context.App.Metadata["timestamp"].(time.Time)

		logger.Info("Regenerating files", zap.Time("timestamp", timestamp))

		return nil
	}

	app.Commands = []*cli.Command{
		{
			//TODO: Refactor subscription-renewal and use personUsecase.On
			Name:  "subscription-renewal",
			Usage: "generate markdown for subscription renewals",
			Flags: append(app.Flags,
				&cli.IntFlag{
					Name:     "year",
					Usage:    "Subscription renewal year",
					Required: true,
				},
			),
			Action: func(context *cli.Context) error {
				subscriptionRenewalYear := context.Int("year")

				personSlice, err := personUsecase.GetPersonSliceAt(time.Date(subscriptionRenewalYear, time.January, 31, 0, 0, 0, 0, time.UTC))
				if err != nil {
					logger.Fatal("--- Persons GetPersonSliceAt: FAILED ---", zap.Error(err))
					return err
				}

				logger.Debug("--- Persons GetPersonSliceAt: SUCCEEDED ---")

				timestamp := context.App.Metadata["timestamp"].(time.Time)

				community, err := communityUsecase.GetCommunityAt(timestamp)
				if err != nil {
					logger.Fatal("--- communityUsecase.GetCommunityAt: FAILED ---", zap.Error(err))
					return err
				}

				logger.Debug("--- communityUsecase.GetCommunityAt: SUCCEEDED ---")

				err = subscriptionRenewalUsecase.GenerateMarkdownSubscriptionRenewal(subscriptionRenewalYear, timestamp, *community, *personSlice)
				if err != nil {
					logger.Fatal("--- subscriptionRenewal GenerateMarkdownSubscriptionRenewal: FAILED ---", zap.Error(err))
					return err
				}

				logger.Debug("--- subscriptionRenewal GenerateMarkdownSubscriptionRenewal: SUCCEEDED ---")

				return nil
			},
		},
		{
			Name:  "add-member-event",
			Usage: "Add an member event",
			Flags: append(app.Flags,
				&cli.StringFlag{
					Name:     "json",
					Usage:    "Member event in json format",
					Required: true,
				},
			),
			Action: func(context *cli.Context) error {
				json := context.String("json")

				memberEvent, err := model.NewMemberEventFromByteSlice([]byte(json))
				if err != nil {
					return nil
				}

				//TODO: refactor to add event to an worker queue
				err = personUsecase.On(*memberEvent)
				if err != nil {
					return nil
				}

				context.App.Metadata["timestamp"] = time.Now().UTC()

				return nil
			},
		},
	}

	app.After = func(context *cli.Context) error {
		timestamp := context.App.Metadata["timestamp"].(time.Time)

		community, err := communityUsecase.GetCommunityAt(timestamp)
		if err != nil {
			logger.Fatal("--- communityUsecase.GetCommunityAt: FAILED ---", zap.Error(err))
			return err
		}

		logger.Debug("--- communityUsecase.GetCommunityAt: SUCCEEDED ---")

		err = communityUsecase.GenerateMarkdownCommunity(*community)
		if err != nil {
			logger.Fatal("--- Community GenerateMarkdownCommunity: FAILED ---", zap.Error(err))
			return err
		}

		logger.Debug("--- Community GenerateMarkdownCommunity: SUCCEEDED ---")

		personSlice, err := personUsecase.GetPersonSliceAt(timestamp)
		if err != nil {
			logger.Fatal("--- Persons GetPersonSliceAt: FAILED ---", zap.Error(err))
			return err
		}

		err = personUsecase.GenerateMarkdownPersons(*personSlice)
		if err != nil {
			logger.Fatal("--- Persons GenerateMarkdownPersons: FAILED ---", zap.Error(err))
			return err
		}

		logger.Debug("--- Persons GenerateMarkdownPersons: SUCCEEDED ---")

		prettier := exec.Command("prettier", ".", "--write")

		prettierOutput, err := prettier.Output()
		if err != nil {
			return err
		}

		for _, outputLine := range strings.Split(string(prettierOutput), "\n") {
			logger.Info(outputLine)
		}

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func newZapLogger() (*zap.Logger, error) {
	config := helper.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
