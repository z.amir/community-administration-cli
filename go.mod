module gitlab.com/z.amir/community-administration-cli

go 1.21.4

require (
	github.com/golang/mock v1.6.0
	github.com/invopop/jsonschema v0.12.0
	github.com/stoewer/go-strcase v1.3.0
	github.com/thessem/zap-prettyconsole v0.3.0
	github.com/urfave/cli/v2 v2.26.0
	github.com/xeipuuv/gojsonschema v1.2.0
	go.uber.org/zap v1.26.0
	sigs.k8s.io/yaml v1.4.0
)

require (
	github.com/Code-Hex/dd v1.1.0 // indirect
	github.com/bahlo/generic-list-go v0.2.0 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.3 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	github.com/wk8/go-ordered-map/v2 v2.1.8 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
