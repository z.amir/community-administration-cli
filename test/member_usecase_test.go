package test

// import (
// 	"bytes"
// 	"fmt"
// 	"os"
// 	"path/filepath"
// 	"strings"
// 	"testing"
// 	"testing/fstest"
// 	"time"

// 	"github.com/golang/mock/gomock"
// 	"gitlab.com/z.amir/community-administration-cli/helper"
// 	"gitlab.com/z.amir/community-administration-cli/internal/model"
// 	"go.uber.org/zap"
// 	"go.uber.org/zap/zaptest/observer"
// 	"gotest.tools/assert"
// )

// func TestValidateAndLoadMemberEventsYamlUsingAnEmpty(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	observedZapCore, _ := observer.New(zap.InfoLevel)
// 	observedLogger := zap.New(observedZapCore)

// 	mockMemberEventRepository := mock_repositories.NewMockMemberEventRepository(controller)
// 	mockMemberEventRepository.EXPECT().Create(gomock.Any()).Times(0)

// 	workDirectory, err := os.Getwd()
// 	if err != nil {
// 		t.Errorf("os.Getwd failed, %v", err)
// 	}

// 	rootDirectory := filepath.Dir(workDirectory)

// 	memberEventschemaFilePath := "schema/member_event.schema.json"

// 	memberEventschemaBytes, err := os.ReadFile(filepath.Join(rootDirectory, memberEventschemaFilePath))
// 	if err != nil {
// 		t.Errorf("failed to read member event schema file: %v", err)
// 	}

// 	fs := fstest.MapFS(fstest.MapFS{
// 		memberEventschemaFilePath: {Data: memberEventschemaBytes},
// 	})

// 	memberEventUsecase := usecases.NewMemberEventUsecase(observedLogger, mockMemberEventRepository, fs)

// 	memberEventsYaml := ""

// 	memberEventsReader := strings.NewReader(memberEventsYaml)

// 	memberEventUsecase.ValidateAndLoadMemberEventsYaml(memberEventsReader)
// }

// func TestValidateAndLoadMemberEventsYamlWithAPersonApproved(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	config := helper.ZapConfig()
// 	logger, err := config.Build()
// 	if err != nil {
// 		t.Errorf("ZapConfig.Build failed: %v", err)
// 	}

// 	eventTimestamp, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
// 	if err != nil {
// 		t.Errorf("time.Parse failed: %v", err)
// 	}

// 	personApproved := model.PersonApproved{
// 		BaseMemberEvent: model.BaseMemberEvent{
// 			MemberEventType: model.MemberEventType{Type: model.PERSON_APPROVED},
// 			Date:            eventTimestamp,
// 		},
// 		Name:  "Mock name",
// 		Email: "mock@email.email",
// 	}

// 	mockMemberEventRepository := mock_repositories.NewMockMemberEventRepository(controller)
// 	mockMemberEventRepository.EXPECT().Create(personApproved).Times(1)

// 	workDirectory, err := os.Getwd()
// 	if err != nil {
// 		t.Errorf("os.Getwd failed, %v", err)
// 	}

// 	rootDirectory := filepath.Dir(workDirectory)

// 	memberEventschemaFilePath := "schema/member_event.schema.json"

// 	memberEventschemaBytes, err := os.ReadFile(filepath.Join(rootDirectory, memberEventschemaFilePath))
// 	if err != nil {
// 		t.Errorf("failed to read member event schema file: %v", err)
// 	}

// 	fs := fstest.MapFS(fstest.MapFS{
// 		memberEventschemaFilePath: {Data: memberEventschemaBytes},
// 	})

// 	memberEventUsecase := usecases.NewMemberEventUsecase(observedLogger, mockMemberEventRepository, fs)

// 	memberEventsYaml := fmt.Sprintf(`- date: "%s"
//   email: "%s"
//   name: %s
//   personId: %d
//   type: %s
// `,
// 		personApproved.Date.Format(time.RFC3339),
// 		personApproved.Email,
// 		personApproved.Name,
// 		personApproved.PersonId,
// 		personApproved.Type,
// 	)

// 	memberEventsReader := strings.NewReader(memberEventsYaml)

// 	memberEventUsecase.ValidateAndLoadMemberEventsYaml(memberEventsReader)
// }

// func TestSaveMemverEventsToYaml(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	config := helper.ZapConfig()
// 	logger, err := config.Build()
// 	if err != nil {
// 		t.Errorf("failed to create new zap logger: %v", err)
// 	}

// 	eventTimestamp, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
// 	if err != nil {
// 		t.Errorf("time.Parse failed: %v", err)
// 	}

// 	mockMemberEventRepository := mock_repositories.NewMockMemberEventRepository(controller)
// 	mockMemberEventRepository.EXPECT().List().Times(1).Return(&[]model.MemberEvent{
// 		model.PersonApproved{
// 			BaseMemberEvent: model.BaseMemberEvent{
// 				MemberEventType: model.MemberEventType{Type: model.PERSON_APPROVED},
// 				Date:            eventTimestamp,
// 			},
// 			Name:  "Mock name",
// 			Email: "mock@email.email",
// 		},
// 	}, nil)

// 	memberEventUsecase := usecases.NewMemberEventUsecase(observedLogger, mockMemberEventRepository, nil)

// 	buffer := bytes.NewBuffer([]byte{})

// 	memberEventUsecase.SaveToYaml(buffer)

// 	yamlBytes := buffer.Bytes()

// 	yamlString := `# yaml-language-server: $schema=https://gitlab.com/z.amir/community-administration-cli/-/raw/main/schema/member_event.schema.json
// - date: "2006-01-02T15:04:05Z"
//   email: mock@email.email
//   name: Mock name
//   personId: 0
//   type: PERSON_APPROVED
// `

// 	assert.Equal(t, string(yamlBytes), yamlString)
// }
