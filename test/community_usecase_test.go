package test

// func TestValidateAndLoadCommunityEventsYamlUsingAnEmpty(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	config := helper.ZapConfig()
// 	logger, err := config.Build()
// 	if err != nil {
// 		t.Errorf("failed to create new zap logger: %v", err)
// 	}

// 	mockCommunityEventRepository := mock_repositories.NewMockCommunityEventRepository(controller)
// 	mockCommunityEventRepository.EXPECT().Create(gomock.Any()).Times(0)

// 	workDirectory, err := os.Getwd()
// 	if err != nil {
// 		t.Errorf("os.Getwd failed, %v", err)
// 	}

// 	rootDirectory := filepath.Dir(workDirectory)

// 	communityEventSchemaFilePath := "schema/community_event.schema.json"

// 	communityEventSchemaBytes, err := os.ReadFile(filepath.Join(rootDirectory, communityEventSchemaFilePath))
// 	if err != nil {
// 		t.Errorf("failed to read community event schema file: %v", err)
// 	}

// 	fs := fstest.MapFS(fstest.MapFS{
// 		communityEventSchemaFilePath: {Data: communityEventSchemaBytes},
// 	})

// 	communityEventUsecase := usecases.NewCommunityEventUsecase(logger, mockCommunityEventRepository, fs)

// 	communityEventsYaml := ""

// 	communityEventsReader := strings.NewReader(communityEventsYaml)

// 	communityEventUsecase.ValidateAndLoadCommunityEventsYaml(communityEventsReader)
// }

// func TestValidateAndLoadCommunityEventsYamlWithACommunityInfoChangedEvent(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	config := helper.ZapConfig()
// 	logger, err := config.Build()
// 	if err != nil {
// 		t.Errorf("ZapConfig.Build failed: %v", err)
// 	}

// 	eventTimestamp, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
// 	if err != nil {
// 		t.Errorf("time.Parse failed: %v", err)
// 	}

// 	communityInfoChanged := model.CommunityInfoChanged{
// 		BaseCommunityEvent: model.BaseCommunityEvent{
// 			CommunityEventType: model.CommunityEventType{
// 				Type: model.COMMUNITY_INFO_CHANGED,
// 			},
// 			Date: eventTimestamp,
// 		},
// 		OrganizationName:        "Mock test community",
// 		ChamberOfCommerceNumber: "1234567",
// 		Iban:                    "GB33BUKB20201555555555",
// 		OrganizationAddress: model.Address{
// 			Street:              "Straatnaam",
// 			Housenumber:         1,
// 			HousenumberAddition: "a",
// 			PostalCode:          "1234AB",
// 			City:                "Stad",
// 		},
// 	}

// 	mockCommunityEventRepository := mock_repositories.NewMockCommunityEventRepository(controller)
// 	mockCommunityEventRepository.EXPECT().Create(communityInfoChanged).Times(1)

// 	workDirectory, err := os.Getwd()
// 	if err != nil {
// 		t.Errorf("os.Getwd failed, %v", err)
// 	}

// 	rootDirectory := filepath.Dir(workDirectory)

// 	communityEventSchemaFilePath := "schema/community_event.schema.json"

// 	communityEventSchemaBytes, err := os.ReadFile(filepath.Join(rootDirectory, communityEventSchemaFilePath))
// 	if err != nil {
// 		t.Errorf("failed to read community event schema file: %v", err)
// 	}

// 	fs := fstest.MapFS(fstest.MapFS{
// 		communityEventSchemaFilePath: {Data: communityEventSchemaBytes},
// 	})

// 	communityEventUsecase := usecases.NewCommunityEventUsecase(logger, mockCommunityEventRepository, fs)

// 	communityEventsYaml := fmt.Sprintf(`- chamberOfCommerceNumber: "%s"
//   date: "%s"
//   iban: %s
//   organizationAddress:
//     city: %s
//     housenumber: %d
//     housenumberAddition: %s
//     postalCode: %s
//     street: %s
//   organizationName: %s
//   type: %s
// `,
// 		communityInfoChanged.ChamberOfCommerceNumber,
// 		communityInfoChanged.Date.Format(time.RFC3339),
// 		communityInfoChanged.Iban,
// 		communityInfoChanged.OrganizationAddress.City,
// 		communityInfoChanged.OrganizationAddress.Housenumber,
// 		communityInfoChanged.OrganizationAddress.HousenumberAddition,
// 		communityInfoChanged.OrganizationAddress.PostalCode,
// 		communityInfoChanged.OrganizationAddress.Street,
// 		communityInfoChanged.OrganizationName,
// 		communityInfoChanged.Type,
// 	)

// 	communityEventsReader := strings.NewReader(communityEventsYaml)

// 	communityEventUsecase.ValidateAndLoadCommunityEventsYaml(communityEventsReader)
// }

// func TestSaveCommunityEventsToYaml(t *testing.T) {
// 	controller := gomock.NewController(t)
// 	defer controller.Finish()

// 	config := helper.ZapConfig()
// 	logger, err := config.Build()
// 	if err != nil {
// 		t.Errorf("failed to create new zap logger: %v", err)
// 	}

// 	eventTimestamp, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
// 	if err != nil {
// 		t.Errorf("time.Parse failed: %v", err)
// 	}

// 	mockCommunityEventRepository := mock_repositories.NewMockCommunityEventRepository(controller)
// 	mockCommunityEventRepository.EXPECT().List().Times(1).Return(&[]model.CommunityEvent{
// 		model.SubscriptionFeeSet{
// 			BaseCommunityEvent: model.BaseCommunityEvent{
// 				CommunityEventType: model.CommunityEventType{
// 					Type: model.SUBSCRIPTION_FEE_SET,
// 				},
// 				Date: eventTimestamp,
// 			},
// 			Year:   2023,
// 			Amount: 100,
// 		},
// 	}, nil)

// 	communityEventUsecase := usecases.NewCommunityEventUsecase(logger, mockCommunityEventRepository, nil)

// 	buffer := bytes.NewBuffer([]byte{})

// 	communityEventUsecase.SaveToYaml(buffer)

// 	yamlBytes := buffer.Bytes()

// 	yamlString := `# yaml-language-server: $schema=https://gitlab.com/z.amir/community-administration-cli/-/raw/main/schema/community_event.schema.json
// - amount: 100
//   date: "2006-01-02T15:04:05Z"
//   type: SUBSCRIPTION_FEE_SET
//   year: 2023
// `

// 	assert.Equal(t, string(yamlBytes), yamlString)
// }
