package model

type CommunityInfoChanged struct {
	BaseCommunityEvent
	OrganizationName        string  `json:"organizationName"`
	ChamberOfCommerceNumber string  `json:"chamberOfCommerceNumber"`
	Iban                    string  `json:"iban"`
	OrganizationAddress     Address `json:"organizationAddress"`
}

type SubscriptionFeeSet struct {
	BaseCommunityEvent
	Year   int `json:"year"`
	Amount int `json:"amount"`
}
