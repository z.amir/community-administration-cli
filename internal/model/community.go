package model

import (
	"fmt"
	"time"
)

type Community struct {
	OrganizationName        string                  `json:"organizationName"`
	ChamberOfCommerceNumber string                  `json:"chamberOfCommerceNumber"`
	Iban                    string                  `json:"iban"`
	OrganizationAddress     Address                 `json:"organizationAddress"`
	Subscriptions           []CommunitySubscription `json:"subscriptions"`

	Events        []CommunityEvent
	DateGenerated time.Time
}

type CommunitySubscription struct {
	Year   int `json:"year"`
	Amount int `json:"amount"`
}

func (community *Community) on(communityEvent CommunityEvent) error {
	switch e := communityEvent.(type) {
	case SubscriptionFeeSet:
		community.Subscriptions = append(
			community.Subscriptions,
			CommunitySubscription{
				Year:   e.Year,
				Amount: e.Amount,
			},
		)
	case CommunityInfoChanged:
		community.OrganizationName = e.OrganizationName
		community.ChamberOfCommerceNumber = e.ChamberOfCommerceNumber
		community.Iban = e.Iban
		community.OrganizationAddress = e.OrganizationAddress
	default:
		return fmt.Errorf("event type not found")
	}

	community.Events = append(community.Events, communityEvent)

	return nil
}

func NewCommunity(communityEventSlice []CommunityEvent, timestamp time.Time) (*Community, error) {
	community := &Community{}

	for _, communityEvent := range communityEventSlice {
		if communityEvent.GetBaseCommunityEvent().Date.After(timestamp) {
			continue
		}
		err := community.on(communityEvent)
		if err != nil {
			return nil, err
		}
	}

	community.DateGenerated = timestamp

	return community, nil
}
