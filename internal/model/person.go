package model

import (
	"fmt"
	"time"
)

type Person struct {
	Id                    int            `json:"id"`
	Name                  string         `json:"name"`
	Email                 string         `json:"email"`
	OrganizationName      string         `json:"organizationName"`
	OrganizationAddress   Address        `json:"organizationAddress"`
	CorrespondenceAddress *Address       `json:"correspondenceAddress,omitempty"`
	Phonenumber           string         `json:"phonenumber"`
	MembershipId          int            `json:"membershipId"`
	MembershipActive      bool           `json:"membershipActive"`
	Subscriptions         []Subscription `json:"subscriptions"`

	Events        []MemberEvent
	DateGenerated time.Time
}

func (person *Person) on(memberEvent MemberEvent) error {
	switch e := memberEvent.(type) {
	case PersonApproved:
		person.Id = e.PersonId
		person.Name = e.Name
		person.Email = e.Email
		person.MembershipActive = false
	case CreatePaymentRequest:
		person.Id = e.PersonId
		person.Subscriptions = append(
			person.Subscriptions,
			Subscription{
				Year:   e.Year,
				Status: OPEN,
			},
		)
	case ApplicationFormSent:
		person.Id = e.PersonId
	case PaymentCompleted:
		person.Id = e.PersonId
	case SubscriptionFormReceived:
		person.Id = e.PersonId
	case SubscriptionPayed:
		var firstUnpayedSubscriptionIndex int
		for index, subscription := range person.Subscriptions {
			if subscription.Status != PAYED {
				firstUnpayedSubscriptionIndex = index
				break
			}
		}

		person.Id = e.PersonId
		person.Subscriptions[firstUnpayedSubscriptionIndex].Status = PAYED
	case MemberInfoCompleted:
		person.Id = e.PersonId
		person.OrganizationName = e.OrganizationName
		person.OrganizationAddress = e.OrganizationAddress
		person.CorrespondenceAddress = e.CorrespondenceAddress
		person.Phonenumber = e.Phonenumber
	case MembershipIdAssigned:
		person.Id = e.PersonId
		person.MembershipId = e.MembershipId
		person.MembershipActive = true
	case PaymentConfirmationLetterCreated:
		person.Id = e.PersonId
	case PaymentConfirmationSent:
		person.Id = e.PersonId
	case WelcomeLetterSent:
		person.Id = e.PersonId
	case MemberInfoChanged:
		person.Id = e.PersonId
		person.Name = e.Name
		person.Email = e.Email
		person.OrganizationName = e.OrganizationName
		person.OrganizationAddress = e.OrganizationAddress
		person.CorrespondenceAddress = e.CorrespondenceAddress
		person.Phonenumber = e.Phonenumber
	case SubscriptionRenewalSent:
		person.Id = e.PersonId
	case MembershipCancellationReceived:
		person.Id = e.PersonId
	case MembershipCancellationConfirmationSent:
		person.Id = e.PersonId
	case MembershipCancelled:
		person.Id = e.PersonId
		person.MembershipActive = false
	default:
		return fmt.Errorf("event type not found: %s", e.GetBaseMemberEvent().Type)
	}

	person.Events = append(person.Events, memberEvent)

	return nil
}

func NewPerson(memberEventSlice []MemberEvent, timestamp time.Time) (*Person, error) {
	person := &Person{}

	for _, memberEvent := range memberEventSlice {
		if memberEvent.GetBaseMemberEvent().Date.After(timestamp) {
			continue
		}
		err := person.on(memberEvent)
		if err != nil {
			return nil, err
		}
	}

	person.DateGenerated = timestamp

	return person, nil
}
