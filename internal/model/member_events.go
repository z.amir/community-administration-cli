package model

type PersonApproved struct {
	BaseMemberEvent
	Name  string `json:"name"`
	Email string `json:"email"`
}

type CreatePaymentRequest struct {
	BaseMemberEvent
	Year int `json:"year"`
}

type ApplicationFormSent struct {
	BaseMemberEvent
}

type PaymentCompleted struct {
	BaseMemberEvent
}

type SubscriptionFormReceived struct {
	BaseMemberEvent
}

type SubscriptionPayed struct {
	BaseMemberEvent
}

type MemberInfoCompleted struct {
	BaseMemberEvent
	OrganizationName      string   `json:"organizationName"`
	OrganizationAddress   Address  `json:"organizationAddress"`
	CorrespondenceAddress *Address `json:"correspondenceAddress,omitempty"`
	Phonenumber           string   `json:"phonenumber"`
}

type MembershipIdAssigned struct {
	BaseMemberEvent
	MembershipId int `json:"membershipId"`
}
type PaymentConfirmationLetterCreated struct {
	BaseMemberEvent
}

type PaymentConfirmationSent struct {
	BaseMemberEvent
}

type WelcomeLetterSent struct {
	BaseMemberEvent
}

type MemberInfoChanged struct {
	BaseMemberEvent
	Name                  string   `json:"name"`
	OrganizationName      string   `json:"organizationName"`
	OrganizationAddress   Address  `json:"organizationAddress"`
	CorrespondenceAddress *Address `json:"correspondenceAddress,omitempty"`
	Email                 string   `json:"email"`
	Phonenumber           string   `json:"phonenumber"`
}

type SubscriptionRenewalSent struct {
	BaseMemberEvent
	Year int `json:"year"`
}

type MembershipCancellationReceived struct {
	BaseMemberEvent
}

type MembershipCancellationConfirmationSent struct {
	BaseMemberEvent
}

type MembershipCancelled struct {
	BaseMemberEvent
}
