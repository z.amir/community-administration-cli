package model

type Address struct {
	Street              string `json:"street"`
	Housenumber         int    `json:"housenumber"`
	HousenumberAddition string `json:"housenumberAddition,omitempty"`
	PostalCode          string `json:"postalCode"`
	City                string `json:"city"`
}
