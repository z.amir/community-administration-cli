package model

type SubscriptionStatus string

const (
	OPEN  SubscriptionStatus = "OPEN"
	PAYED SubscriptionStatus = "BETAALD"
)

type Subscription struct {
	Year   int                `json:"year"`
	Status SubscriptionStatus `json:"status"`
}
