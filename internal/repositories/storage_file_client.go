package repositories

import (
	"io/fs"
	"os"
	"path/filepath"

	"go.uber.org/zap"
)

type StorageFileClient struct {
	Logger                      *zap.Logger
	AdministrationDirectoryPath string
}

func NewStorageFileClient(logger *zap.Logger, administrationDirectoryPath string) *StorageFileClient {
	return &StorageFileClient{
		Logger:                      logger,
		AdministrationDirectoryPath: administrationDirectoryPath,
	}
}

func (c *StorageFileClient) ReadFile(name string) ([]byte, error) {
	readFilePath := filepath.Join(c.AdministrationDirectoryPath, name)

	return os.ReadFile(readFilePath)
}

func (c *StorageFileClient) WriteFile(name string, data []byte, perm fs.FileMode) error {
	writeFilePath := filepath.Join(c.AdministrationDirectoryPath, name)

	return os.WriteFile(writeFilePath, data, perm)
}

func (c *StorageFileClient) MkdirAll(path string, perm fs.FileMode) error {
	mkdirAllPath := filepath.Join(c.AdministrationDirectoryPath, path)

	return os.MkdirAll(mkdirAllPath, perm)
}

func (c *StorageFileClient) RemoveAll(path string) error {
	removeAllPath := filepath.Join(c.AdministrationDirectoryPath, path)

	return os.RemoveAll(removeAllPath)
}

func (c *StorageFileClient) Remove(name string) error {
	removePath := filepath.Join(c.AdministrationDirectoryPath, name)

	return os.Remove(removePath)
}
