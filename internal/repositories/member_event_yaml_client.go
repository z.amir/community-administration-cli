package repositories

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"sort"

	"github.com/xeipuuv/gojsonschema"
	"go.uber.org/zap"
	"sigs.k8s.io/yaml"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
)

type MemberEventYamlClient struct {
	Logger                   *zap.Logger
	SchemaFileSystem         fs.ReadFileFS
	MemberEventsYamlFilePath string
}

func NewMemberEventYamlClient(logger *zap.Logger, schemaFileSystem fs.ReadFileFS, memberEventsYamlFilePath string) *MemberEventYamlClient {
	return &MemberEventYamlClient{
		Logger:                   logger,
		SchemaFileSystem:         schemaFileSystem,
		MemberEventsYamlFilePath: memberEventsYamlFilePath,
	}
}

func (c *MemberEventYamlClient) Create(memberEvent model.MemberEvent) error {
	memberEventSlice, err := c.validateAndLoadMemberEventsYaml()
	if err != nil {
		return err
	}

	memberEventSliceUpdated := *memberEventSlice

	memberEventSliceUpdated = append(memberEventSliceUpdated, memberEvent)

	err = c.saveToYaml(memberEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *MemberEventYamlClient) List() (*[]model.MemberEvent, error) {
	memberEventSlice, err := c.validateAndLoadMemberEventsYaml()
	if err != nil {
		return nil, err
	}

	return memberEventSlice, nil
}

func (c *MemberEventYamlClient) Get(baseMemberEvent model.BaseMemberEvent) (*model.MemberEvent, error) {
	memberEventSlice, err := c.validateAndLoadMemberEventsYaml()
	if err != nil {
		return nil, err
	}

	var result model.MemberEvent
	for _, memberEvent := range *memberEventSlice {
		if memberEvent.GetBaseMemberEvent() == baseMemberEvent {
			result = memberEvent
			break
		}
	}
	if result == nil {
		return nil, ErrMemberEventNotFound
	}

	return &result, nil
}

func (c *MemberEventYamlClient) Update(updatedMemberEvent model.MemberEvent, baseMemberEventToUpdate model.BaseMemberEvent) error {
	memberEventSlice, err := c.validateAndLoadMemberEventsYaml()
	if err != nil {
		return err
	}

	memberEventSliceUpdated := *memberEventSlice

	var foundIndex *int
	for index, event := range memberEventSliceUpdated {
		if event.GetBaseMemberEvent() == baseMemberEventToUpdate {
			foundIndex = &index
			break
		}
	}

	if foundIndex == nil {
		return ErrMemberEventNotFound
	}

	memberEventSliceUpdated[*foundIndex] = updatedMemberEvent

	err = c.saveToYaml(memberEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *MemberEventYamlClient) Delete(baseMemberEvent model.BaseMemberEvent) error {
	memberEventSlice, err := c.validateAndLoadMemberEventsYaml()
	if err != nil {
		return err
	}

	memberEventSliceUpdated := *memberEventSlice

	var foundIndex *int
	for index, event := range memberEventSliceUpdated {
		if event.GetBaseMemberEvent() == baseMemberEvent {
			foundIndex = &index
			break
		}
	}

	if foundIndex == nil {
		return ErrMemberEventNotFound
	}

	memberEventSliceUpdated = append(memberEventSliceUpdated[:*foundIndex], memberEventSliceUpdated[*foundIndex+1:]...)

	err = c.saveToYaml(memberEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *MemberEventYamlClient) validateAndLoadMemberEventsYaml() (*[]model.MemberEvent, error) {
	yamlBytes, err := os.ReadFile(c.MemberEventsYamlFilePath)
	if err != nil {
		return nil, err
	}

	jsonBytes, err := yaml.YAMLToJSON(yamlBytes)
	if err != nil {
		return nil, err
	}

	schemaBytes, err := c.SchemaFileSystem.ReadFile("schema/member_event.schema.json")
	if err != nil {
		return nil, err
	}

	schemaLoader := gojsonschema.NewBytesLoader(schemaBytes)

	documentLoader := gojsonschema.NewBytesLoader(jsonBytes)

	validator, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	if !validator.Valid() {
		c.Logger.Error("schema validation failed", zap.Reflect("validator errors", validator.Errors()))
		return nil, fmt.Errorf("invalid member event yaml")
	}

	rawMessageSlice := []json.RawMessage{}
	err = json.Unmarshal(jsonBytes, &rawMessageSlice)
	if err != nil {
		return nil, err
	}

	memberEventSlice := []model.MemberEvent{}

	for _, rawMessage := range rawMessageSlice {
		memberEvent, err := model.NewMemberEventFromByteSlice(rawMessage)
		if err != nil {
			return nil, err
		}

		memberEventSlice = append(memberEventSlice, *memberEvent)

		if err != nil {
			return nil, err
		}
	}

	return &memberEventSlice, nil
}

func (c *MemberEventYamlClient) saveToYaml(memberEventSlice []model.MemberEvent) error {
	sort.SliceStable(memberEventSlice, func(i, j int) bool {
		return memberEventSlice[i].GetBaseMemberEvent().Date.Before(memberEventSlice[j].GetBaseMemberEvent().Date)
	})

	yamlBytes, err := yaml.Marshal(&memberEventSlice)
	if err != nil {
		return err
	}

	yamlString := string(yamlBytes)

	yamlString = "# yaml-language-server: $schema=https://gitlab.com/z.amir/community-administration-cli/-/raw/main/schema/member_event.schema.json\n" + yamlString

	err = os.WriteFile(c.MemberEventsYamlFilePath, []byte(yamlString), 0644)

	if err != nil {
		return err
	}

	return nil
}
