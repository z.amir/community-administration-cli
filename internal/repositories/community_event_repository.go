package repositories

import (
	"errors"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
)

type CommunityEventRepository interface {
	Create(event model.CommunityEvent) error
	List() (*[]model.CommunityEvent, error)
	Get(baseEvent model.BaseCommunityEvent) (*model.CommunityEvent, error)
	Update(updatedEvent model.CommunityEvent, baseEventToUpdate model.BaseCommunityEvent) error
	Delete(baseEvent model.BaseCommunityEvent) error
}

var ErrCommunityEventNotFound = errors.New("event not found error")
