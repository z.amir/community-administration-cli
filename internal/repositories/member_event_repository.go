package repositories

import (
	"errors"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
)

type MemberEventRepository interface {
	Create(event model.MemberEvent) error
	List() (*[]model.MemberEvent, error)
	Get(baseEvent model.BaseMemberEvent) (*model.MemberEvent, error)
	Update(updatedEvent model.MemberEvent, baseEventToUpdate model.BaseMemberEvent) error
	Delete(baseEvent model.BaseMemberEvent) error
}

var ErrMemberEventNotFound = errors.New("event not found error")
