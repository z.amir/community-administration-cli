package repositories

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"sort"

	"github.com/xeipuuv/gojsonschema"
	"go.uber.org/zap"
	"sigs.k8s.io/yaml"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
)

type CommunityEventYamlClient struct {
	Logger                      *zap.Logger
	SchemaFileSystem            fs.ReadFileFS
	CommunityEventsYamlFilePath string
}

func NewCommunityEventYamlClient(logger *zap.Logger, schemaFileSystem fs.ReadFileFS, communityEventsYamlFilePath string) *CommunityEventYamlClient {
	return &CommunityEventYamlClient{
		Logger:                      logger,
		SchemaFileSystem:            schemaFileSystem,
		CommunityEventsYamlFilePath: communityEventsYamlFilePath,
	}
}

func (c *CommunityEventYamlClient) Create(communityEvent model.CommunityEvent) error {
	communityEventSlice, err := c.validateAndLoadCommunityEventsYaml()
	if err != nil {
		return err
	}

	communityEventSliceUpdated := *communityEventSlice

	communityEventSliceUpdated = append(communityEventSliceUpdated, communityEvent)

	err = c.saveToYaml(communityEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *CommunityEventYamlClient) List() (*[]model.CommunityEvent, error) {
	communityEventSlice, err := c.validateAndLoadCommunityEventsYaml()
	if err != nil {
		return nil, err
	}

	return communityEventSlice, nil
}

func (c *CommunityEventYamlClient) Get(baseCommunityEvent model.BaseCommunityEvent) (*model.CommunityEvent, error) {
	communityEventSlice, err := c.validateAndLoadCommunityEventsYaml()
	if err != nil {
		return nil, err
	}

	var result model.CommunityEvent
	for _, communityEvent := range *communityEventSlice {
		if communityEvent.GetBaseCommunityEvent() == baseCommunityEvent {
			result = communityEvent
			break
		}
	}
	if result == nil {
		return nil, ErrCommunityEventNotFound
	}

	return &result, nil
}

func (c *CommunityEventYamlClient) Update(updatedCommunityEvent model.CommunityEvent, baseCommunityEventToUpdate model.BaseCommunityEvent) error {
	communityEventSlice, err := c.validateAndLoadCommunityEventsYaml()
	if err != nil {
		return err
	}

	communityEventSliceUpdated := *communityEventSlice

	var foundIndex *int
	for index, event := range communityEventSliceUpdated {
		if event.GetBaseCommunityEvent() == baseCommunityEventToUpdate {
			foundIndex = &index
			break
		}
	}

	if foundIndex == nil {
		return ErrCommunityEventNotFound
	}

	communityEventSliceUpdated[*foundIndex] = updatedCommunityEvent

	err = c.saveToYaml(communityEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *CommunityEventYamlClient) Delete(baseCommunityEvent model.BaseCommunityEvent) error {
	communityEventSlice, err := c.validateAndLoadCommunityEventsYaml()
	if err != nil {
		return err
	}

	communityEventSliceUpdated := *communityEventSlice

	var foundIndex *int
	for index, event := range *communityEventSlice {
		if event.GetBaseCommunityEvent() == baseCommunityEvent {
			foundIndex = &index
			break
		}
	}

	if foundIndex == nil {
		return ErrCommunityEventNotFound
	}

	communityEventSliceUpdated = append(communityEventSliceUpdated[:*foundIndex], communityEventSliceUpdated[*foundIndex+1:]...)

	err = c.saveToYaml(communityEventSliceUpdated)
	if err != nil {
		return err
	}

	return nil
}

func (c *CommunityEventYamlClient) validateAndLoadCommunityEventsYaml() (*[]model.CommunityEvent, error) {
	yamlBytes, err := os.ReadFile(c.CommunityEventsYamlFilePath)
	if err != nil {
		return nil, err
	}

	jsonBytes, err := yaml.YAMLToJSON(yamlBytes)
	if err != nil {
		return nil, err
	}

	schemaBytes, err := c.SchemaFileSystem.ReadFile("schema/community_event.schema.json")
	if err != nil {
		return nil, err
	}

	schemaLoader := gojsonschema.NewBytesLoader(schemaBytes)

	documentLoader := gojsonschema.NewBytesLoader(jsonBytes)

	validator, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	if !validator.Valid() {
		c.Logger.Error("schema validation failed", zap.Reflect("validator errors", validator.Errors()))
		return nil, fmt.Errorf("invalid community event yaml")

	}

	rawMessageSlice := []json.RawMessage{}
	err = json.Unmarshal(jsonBytes, &rawMessageSlice)
	if err != nil {
		return nil, err
	}

	communityEventSlice := []model.CommunityEvent{}

	for _, rawMessage := range rawMessageSlice {
		communityEvent, err := model.NewCommunityEventFromByteSlice(rawMessage)
		if err != nil {
			return nil, err
		}

		communityEventSlice = append(communityEventSlice, *communityEvent)

		if err != nil {
			return nil, err
		}
	}

	return &communityEventSlice, nil
}

func (c *CommunityEventYamlClient) saveToYaml(communityEventSlice []model.CommunityEvent) error {
	sort.SliceStable(communityEventSlice, func(i, j int) bool {
		return communityEventSlice[i].GetBaseCommunityEvent().Date.Before(communityEventSlice[j].GetBaseCommunityEvent().Date)
	})

	yamlBytes, err := yaml.Marshal(&communityEventSlice)
	if err != nil {
		return err
	}

	yamlString := string(yamlBytes)

	yamlString = "# yaml-language-server: $schema=https://gitlab.com/z.amir/community-administration-cli/-/raw/main/schema/community_event.schema.json\n" + yamlString

	err = os.WriteFile(c.CommunityEventsYamlFilePath, []byte(yamlString), 0644)
	if err != nil {
		return err
	}

	return nil
}
