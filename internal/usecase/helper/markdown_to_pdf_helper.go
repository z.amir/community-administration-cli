package helper

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"go.uber.org/zap"
)

func MarkdownToPdf(logger zap.Logger, markdownBytes []byte) ([]byte, error) {
	tempDir, err := os.MkdirTemp("", "dir")
	if err != nil {
		logger.Error("MkdirTemp failed", zap.Error(err))
		return nil, err
	}
	defer os.RemoveAll(tempDir)

	tempOutputFileMarkdownPath := filepath.Join(
		tempDir,
		"output.md",
	)

	err = os.WriteFile(tempOutputFileMarkdownPath, markdownBytes, 0644)
	if err != nil {
		logger.Error("WriteFile failed", zap.Error(err))
		return nil, err
	}

	prettier := exec.Command("prettier", tempOutputFileMarkdownPath, "--write")

	prettierOutput, err := prettier.Output()
	if err != nil {
		return nil, err
	}

	for _, outputLine := range strings.Split(string(prettierOutput), "\n") {
		logger.Info(outputLine)
	}

	tempOutputFilePdfPath := filepath.Join(
		tempDir,
		"output.pdf",
	)

	pandoc := exec.Command("pandoc", "-s", "-V", "geometry:margin=1in", tempOutputFileMarkdownPath, "-o", tempOutputFilePdfPath)

	pandocOutput, err := pandoc.Output()
	for _, outputLine := range strings.Split(string(pandocOutput), "\n") {
		logger.Info(outputLine)
	}
	if err != nil {
		logger.Error("pandoc failed", zap.Error(err))
		return nil, err
	}

	pdfFile, err := os.ReadFile(tempOutputFilePdfPath)
	if err != nil {
		logger.Error("ReadFile failed", zap.Error(err))
		return nil, err
	}

	return pdfFile, nil

}
