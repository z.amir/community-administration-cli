package usecases

import (
	"bytes"
	"fmt"
	"path/filepath"
	"text/template"
	"time"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
	"gitlab.com/z.amir/community-administration-cli/internal/repositories"
	"gitlab.com/z.amir/community-administration-cli/internal/usecase/helper"

	"go.uber.org/zap"
)

type SubscriptionRenewalUsecase struct {
	Logger            *zap.Logger
	StorageRepository repositories.StorageRepository
}

func NewSubscriptionRenewalUsecase(
	logger *zap.Logger,
	storageRepository repositories.StorageRepository,
) *SubscriptionRenewalUsecase {
	return &SubscriptionRenewalUsecase{
		Logger:            logger,
		StorageRepository: storageRepository,
	}
}

func (uc *SubscriptionRenewalUsecase) GenerateMarkdownSubscriptionRenewal(subscriptionRenewalYear int, timestamp time.Time, community model.Community, personSlice []model.Person) error {
	communitySubscription := model.CommunitySubscription{}

	for _, subscription := range community.Subscriptions {
		if subscription.Year == subscriptionRenewalYear {
			communitySubscription = subscription
		}
	}

	divideFuncMap := template.FuncMap{"divide": func(a, b int) int {
		return a / b
	}}

	templateMarkdown, err := uc.StorageRepository.ReadFile(filepath.Join("template", "markdown", "subscription_renewal",
		fmt.Sprintf("%d.template.md", subscriptionRenewalYear)))
	if err != nil {
		uc.Logger.Error("read file failed", zap.Error(err))
		return err
	}

	templateFile, err := template.New("subscription_renewal").Funcs(divideFuncMap).Parse(string(templateMarkdown))
	if err != nil {
		uc.Logger.Error("template parse failed", zap.Error(err))
		return err
	}

	_ = uc.StorageRepository.RemoveAll(filepath.Join("output", "letter", "subscription_renewal",
		fmt.Sprintf("%d", subscriptionRenewalYear)))

	err = uc.StorageRepository.MkdirAll(filepath.Join("output", "letter", "subscription_renewal",
		fmt.Sprintf("%d", subscriptionRenewalYear)), 0755)
	if err != nil {
		uc.Logger.Error("mkdirall failed", zap.Error(err))
		return err
	}

	type TemplateData struct {
		Date                  time.Time
		CommunitySubscription model.CommunitySubscription
		Community             model.Community
		Person                model.Person
	}

	outputFileDirectory := filepath.Join("output", "letter", "subscription_renewal",
		fmt.Sprintf("%d", subscriptionRenewalYear))

	for _, person := range personSlice {
		if !person.MembershipActive {
			continue
		}

		templateData := TemplateData{
			Date:                  timestamp,
			CommunitySubscription: communitySubscription,
			Community:             community,
			Person:                person,
		}

		buffer := bytes.NewBuffer([]byte{})

		err = templateFile.Execute(
			buffer,
			templateData,
		)
		if err != nil {
			uc.Logger.Error("templateFile.Execute failed", zap.Error(err))
			return err
		}

		pdfBytes, err := helper.MarkdownToPdf(*uc.Logger, buffer.Bytes())
		if err != nil {
			uc.Logger.Error("MarkdownToPdf failed", zap.Error(err))
			return err
		}

		outputFilePdfPath := filepath.Join(outputFileDirectory,
			fmt.Sprintf("%s Brief verlenging lidmaatschap %s %s.pdf", timestamp.Local().Format("20060102"), community.OrganizationName, person.Name))

		err = uc.StorageRepository.WriteFile(
			outputFilePdfPath,
			pdfBytes,
			0644,
		)
		if err != nil {
			uc.Logger.Error("WriteFile failed", zap.Error(err))
			return err
		}
	}

	return nil
}
