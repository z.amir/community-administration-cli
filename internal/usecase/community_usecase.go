package usecases

import (
	"bytes"
	"path/filepath"
	"text/template"
	"time"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
	"gitlab.com/z.amir/community-administration-cli/internal/repositories"

	"go.uber.org/zap"
)

type CommunityUsecase struct {
	Logger                   *zap.Logger
	CommunityEventRepository repositories.CommunityEventRepository
	StorageRepository        repositories.StorageRepository
}

func NewCommunityUsecase(
	logger *zap.Logger,
	communityEventRepository repositories.CommunityEventRepository,
	storageRepository repositories.StorageRepository,
) *CommunityUsecase {
	return &CommunityUsecase{
		Logger:                   logger,
		CommunityEventRepository: communityEventRepository,
		StorageRepository:        storageRepository,
	}
}

func (uc *CommunityUsecase) GetCommunityAt(timestamp time.Time) (*model.Community, error) {
	communityEventSlice, err := uc.CommunityEventRepository.List()
	if err != nil {
		return nil, err
	}

	community, err := model.NewCommunity(*communityEventSlice, timestamp)
	if err != nil {
		return nil, err
	}

	return community, nil
}

func (uc *CommunityUsecase) GenerateMarkdownCommunity(community model.Community) error {
	templateMarkdown, err := uc.StorageRepository.ReadFile(filepath.Join("template", "markdown", "community.template.md"))
	if err != nil {
		return err
	}

	templateFile, err := template.New("community").Parse(string(templateMarkdown))
	if err != nil {
		return err
	}

	_ = uc.StorageRepository.Remove(filepath.Join("output", "community.md"))

	err = uc.StorageRepository.MkdirAll(filepath.Join("output"), 0755)
	if err != nil {
		return err
	}

	buffer := bytes.NewBuffer([]byte{})

	err = templateFile.Execute(
		buffer,
		community,
	)
	if err != nil {
		return err
	}

	err = uc.StorageRepository.WriteFile(filepath.Join("output", "community.md"), buffer.Bytes(), 0644)
	if err != nil {
		return err
	}

	return nil
}
