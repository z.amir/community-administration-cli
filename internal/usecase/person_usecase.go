package usecases

import (
	"bytes"
	"fmt"
	"path/filepath"
	"text/template"
	"time"

	"gitlab.com/z.amir/community-administration-cli/internal/model"
	"gitlab.com/z.amir/community-administration-cli/internal/repositories"
	"gitlab.com/z.amir/community-administration-cli/internal/usecase/helper"

	"go.uber.org/zap"
)

type PersonUsecase struct {
	Logger                   *zap.Logger
	CommunityEventRepository repositories.CommunityEventRepository
	MemberEventRepository    repositories.MemberEventRepository
	StorageRepository        repositories.StorageRepository
}

func NewPersonUsecase(
	logger *zap.Logger,
	communityEventRepository repositories.CommunityEventRepository,
	memberEventRepository repositories.MemberEventRepository,
	storageRepository repositories.StorageRepository,
) *PersonUsecase {
	return &PersonUsecase{
		Logger:                   logger,
		CommunityEventRepository: communityEventRepository,
		MemberEventRepository:    memberEventRepository,
		StorageRepository:        storageRepository,
	}
}

func (uc *PersonUsecase) GetPersonSliceAt(timestamp time.Time) (*[]model.Person, error) {
	personSlice := []model.Person{}

	memberEventSlice, err := uc.MemberEventRepository.List()
	if err != nil {
		return nil, err
	}

	memberEventSliceMapById := make(map[int][]model.MemberEvent)
	for _, event := range *memberEventSlice {
		memberEventSliceMapById[event.GetBaseMemberEvent().PersonId] = append(memberEventSliceMapById[event.GetBaseMemberEvent().PersonId], event)
	}

	for _, eventSlice := range memberEventSliceMapById {
		person, err := model.NewPerson(eventSlice, timestamp)
		if err != nil {
			return nil, err
		}

		personSlice = append(personSlice, *person)
	}

	return &personSlice, nil
}

func (uc *PersonUsecase) GetPersonAt(personId int, timestamp time.Time) (*model.Person, error) {
	memberEventSlice, err := uc.MemberEventRepository.List()
	if err != nil {
		return nil, err
	}

	memberEventPersonSlice := []model.MemberEvent{}
	for _, event := range *memberEventSlice {
		if event.GetBaseMemberEvent().PersonId == personId {
			memberEventPersonSlice = append(memberEventPersonSlice, event)
		}
	}

	person, err := model.NewPerson(memberEventPersonSlice, timestamp)
	if err != nil {
		return nil, err
	}

	return person, nil
}

func (uc *PersonUsecase) GenerateMarkdownPersons(personSlice []model.Person) error {
	templateMarkdown, err := uc.StorageRepository.ReadFile(filepath.Join("template", "markdown", "person.template.md"))
	if err != nil {
		return err
	}

	templateFile, err := template.New("person").Parse(string(templateMarkdown))
	if err != nil {
		return err
	}

	_ = uc.StorageRepository.RemoveAll(filepath.Join("output", "person"))

	err = uc.StorageRepository.MkdirAll(filepath.Join("output", "person"), 0755)
	if err != nil {
		return err
	}

	for _, person := range personSlice {
		buffer := bytes.NewBuffer([]byte{})

		err = templateFile.Execute(
			buffer,
			person,
		)
		if err != nil {
			return err
		}

		err = uc.StorageRepository.WriteFile(
			filepath.Join(
				"output",
				"person",
				fmt.Sprintf("%d_%s.md", person.Id, person.Name),
			),
			buffer.Bytes(),
			0644,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *PersonUsecase) On(memberEvent model.MemberEvent) error {
	err := uc.MemberEventRepository.Create(memberEvent)
	if err != nil {
		return err
	}

	switch e := memberEvent.(type) {
	case model.PaymentCompleted:
		err = uc.onPaymentCompleted(e)
		if err != nil {
			return err
		}
	case model.SubscriptionPayed:
		err = uc.onSubscriptionPayed(e)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *PersonUsecase) onPaymentCompleted(paymentCompleted model.PaymentCompleted) error {
	subscriptionPayed := model.SubscriptionPayed{
		BaseMemberEvent: model.BaseMemberEvent{
			MemberEventType: model.MemberEventType{
				Type: model.SUBSCRIPTION_PAYED,
			},
			Date:     time.Now().UTC(),
			PersonId: paymentCompleted.PersonId,
		},
	}

	err := uc.On(subscriptionPayed)
	if err != nil {
		return err
	}

	return nil
}

func (uc *PersonUsecase) onSubscriptionPayed(subscriptionPayed model.SubscriptionPayed) error {
	//TODO: check if full amount is payed

	paymentConfirmationLetterCreated := model.PaymentConfirmationLetterCreated{
		BaseMemberEvent: model.BaseMemberEvent{
			MemberEventType: model.MemberEventType{
				Type: model.PAYMENT_CONFIRMATION_LETTER_CREATED,
			},
			Date:     time.Now().UTC(),
			PersonId: subscriptionPayed.PersonId,
		},
	}

	err := uc.GeneratePaymentConfirmationLetter(paymentConfirmationLetterCreated.PersonId, paymentConfirmationLetterCreated.Date)
	if err != nil {
		return err
	}

	err = uc.On(paymentConfirmationLetterCreated)
	if err != nil {
		return err
	}

	return nil
}

func (uc *PersonUsecase) GeneratePaymentConfirmationLetter(personId int, timestamp time.Time) error {
	communityEventSlice, err := uc.CommunityEventRepository.List()
	if err != nil {
		return err
	}

	community, err := model.NewCommunity(*communityEventSlice, timestamp)
	if err != nil {
		return err
	}

	person, err := uc.GetPersonAt(personId, timestamp)
	if err != nil {
		return err
	}

	var latestPayedSubscriptionYear int
	for _, subscription := range person.Subscriptions {
		if subscription.Status == model.PAYED {
			latestPayedSubscriptionYear = subscription.Year
		}
	}

	payedCommunitySubscription := model.CommunitySubscription{}
	for _, communitySubscription := range community.Subscriptions {
		if communitySubscription.Year == latestPayedSubscriptionYear {
			payedCommunitySubscription = communitySubscription
		}
	}

	divideFuncMap := template.FuncMap{"divide": func(a, b int) int {
		return a / b
	}}

	templateMarkdown, err := uc.StorageRepository.ReadFile(filepath.Join("template", "markdown", "payment_confirmation_letter.template.md"))
	if err != nil {
		uc.Logger.Error("read file failed", zap.Error(err))
		return err
	}

	templateFile, err := template.New("payment_confirmation_letter").Funcs(divideFuncMap).Parse(string(templateMarkdown))
	if err != nil {
		uc.Logger.Error("templateFile.New", zap.Error(err))
		return err
	}

	err = uc.StorageRepository.MkdirAll(filepath.Join("output", "letter", "payment_confirmation_letter"), 0755)
	if err != nil {
		uc.Logger.Error("os.MkdirAll", zap.Error(err))
		return err
	}

	type TemplateData struct {
		Date                  time.Time
		CommunitySubscription model.CommunitySubscription
		Community             model.Community
		Person                model.Person
	}

	templateData := TemplateData{
		Date:                  timestamp,
		CommunitySubscription: payedCommunitySubscription,
		Community:             *community,
		Person:                *person,
	}

	buffer := bytes.NewBuffer([]byte{})

	err = templateFile.ExecuteTemplate(
		buffer,
		"payment_confirmation_letter",
		templateData,
	)
	if err != nil {
		uc.Logger.Error("templateFile.ExecuteTemplate", zap.Error(err))
		return err
	}

	outputFileDirectory := filepath.Join("output", "letter", "payment_confirmation_letter")

	pdfBytes, err := helper.MarkdownToPdf(*uc.Logger, buffer.Bytes())
	if err != nil {
		uc.Logger.Error("MarkdownToPdf failed", zap.Error(err))
		return err
	}

	outputFilePdfPath := filepath.Join(outputFileDirectory,
		fmt.Sprintf("%s Betalingsbevestiging contributie %d - %s.pdf", timestamp.Local().Format("20060102"), latestPayedSubscriptionYear, person.Name))

	err = uc.StorageRepository.WriteFile(
		outputFilePdfPath,
		pdfBytes,
		0644,
	)
	if err != nil {
		uc.Logger.Error("WriteFile failed", zap.Error(err))
		return err
	}

	return nil
}
