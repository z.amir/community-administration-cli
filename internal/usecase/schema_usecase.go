package usecases

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/z.amir/community-administration-cli/internal/model"

	"github.com/invopop/jsonschema"
	"github.com/stoewer/go-strcase"
	"github.com/xeipuuv/gojsonschema"
	"go.uber.org/zap"
	"sigs.k8s.io/yaml"
)

type SchemaUsecase struct {
	Logger *zap.Logger
}

func NewSchemaUsecase(
	logger *zap.Logger,
) *SchemaUsecase {
	return &SchemaUsecase{
		Logger: logger,
	}
}

func (uc *SchemaUsecase) GenerateEventSchema() error {
	reflector := new(jsonschema.Reflector)

	reflector.KeyNamer = strcase.LowerCamelCase
	reflector.BaseSchemaID = "https://community-administration-cli.nl/schema"

	if err := reflector.AddGoComments("gitlab.com/z.amir/community-administration-cli", "./"); err != nil {
		return err
	}

	schema := reflector.Reflect(&[]model.TypeEventSchema{})

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile(filepath.Join("schema", "event_type.schema.json"), bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (uc *SchemaUsecase) ValidateAndUnmarshalTypeEventSchemaYaml(name string) ([]model.TypeEventSchema, error) {
	schemaBytes, err := os.ReadFile(filepath.Join("schema", "event_type.schema.json"))
	if err != nil {
		return nil, err
	}

	yamlBytes, err := os.ReadFile(filepath.Join("data", fmt.Sprintf("%s_event_types.yaml", name)))
	if err != nil {
		return nil, err
	}

	jsonBytes, err := yaml.YAMLToJSON(yamlBytes)
	if err != nil {
		return nil, err
	}

	schemaLoader := gojsonschema.NewStringLoader(string(schemaBytes))

	documentLoader := gojsonschema.NewStringLoader(string(jsonBytes))

	validator, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	if !validator.Valid() {
		return nil, err
	}

	typeEventSchemaSlice := []model.TypeEventSchema{}
	err = json.Unmarshal(jsonBytes, &typeEventSchemaSlice)
	if err != nil {
		return nil, err
	}

	return typeEventSchemaSlice, nil
}

func (uc *SchemaUsecase) GenerateGoCode(name string, typeEventSchemaSlice []model.TypeEventSchema) error {
	templateFileEventType, err := template.ParseFiles(filepath.Join("template", "go", fmt.Sprintf("%s_event_type.go.template", name)))
	if err != nil {
		return err
	}

	type TemplateData struct {
		UpperCase  string
		PascalCase string
	}

	templateDataSlice := []TemplateData{}
	for _, typeEventSchema := range typeEventSchemaSlice {

		replaced := strings.ReplaceAll(typeEventSchema.Type, " ", "_")

		templateData := TemplateData{
			UpperCase:  strings.ToUpper(replaced),
			PascalCase: strcase.UpperCamelCase(replaced),
		}
		templateDataSlice = append(templateDataSlice, templateData)
	}

	outputFileEventType, err := os.Create(filepath.Join("internal", "model", fmt.Sprintf("%s_event_type.go", name)))
	if err != nil {
		return err
	}
	defer outputFileEventType.Close()

	err = templateFileEventType.Execute(
		outputFileEventType,
		templateDataSlice,
	)
	if err != nil {
		return err
	}

	templateFileBaseEvent, err := template.ParseFiles(filepath.Join("template", "go", fmt.Sprintf("base_%s_event.go.template", name)))
	if err != nil {
		return err
	}

	outputFileBaseEvent, err := os.Create(filepath.Join("internal", "model", fmt.Sprintf("base_%s_event.go", name)))
	if err != nil {
		return err
	}
	defer outputFileEventType.Close()

	err = templateFileBaseEvent.Execute(
		outputFileBaseEvent,
		templateDataSlice,
	)
	if err != nil {
		return err
	}

	return nil
}

func (uc *SchemaUsecase) GenerateCommunitySchema() error {
	reflectedEventTypeStructSlice, err := model.GetReflectedCommunityEventTypeStructSlice()
	if err != nil {
		return err
	}

	schema := jsonschema.Schema{
		Version:     "https://json-schema.org/draft/2020-12/schema",
		ID:          "https://community-administration-cli.nl/schema",
		Definitions: jsonschema.Definitions{},
		Items:       &jsonschema.Schema{},
		Type:        "array",
	}

	for _, reflectEvent := range *reflectedEventTypeStructSlice {
		schema.Items.AnyOf = append(schema.Items.AnyOf, &jsonschema.Schema{Ref: reflectEvent.Ref})

		for key, definition := range reflectEvent.Definitions {
			schema.Definitions[key] = definition
		}
	}

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile(filepath.Join("schema", "community_event.schema.json"), bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (uc *SchemaUsecase) GeneratePersonsSchema() error {
	reflectedEventTypeStructSlice, err := model.GetReflectedMemberEventTypeStructSlice()
	if err != nil {
		return err
	}

	schema := jsonschema.Schema{
		Version:     "https://json-schema.org/draft/2020-12/schema",
		ID:          "https://community-administration-cli.nl/schema",
		Definitions: jsonschema.Definitions{},
		Items:       &jsonschema.Schema{},
		Type:        "array",
	}

	for _, reflectEvent := range *reflectedEventTypeStructSlice {
		schema.Items.AnyOf = append(schema.Items.AnyOf, &jsonschema.Schema{Ref: reflectEvent.Ref})

		for key, definition := range reflectEvent.Definitions {
			schema.Definitions[key] = definition
		}
	}

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile(filepath.Join("schema", "member_event.schema.json"), bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}
