cd "$(dirname "$0")"

go run cmd/build/main.go

mockgen -destination=test/mock/community_event_repository.go -source=internal/repositories/community_event_repository.go
mockgen -destination=test/mock/member_event_repository.go -source=internal/repositories/member_event_repository.go
mockgen -destination=test/mock/storage_repository.go -source=internal/repositories/storage_repository.go

mkdir tmp-coverage-reports

go test ./test/... -v -coverpkg=./internal/... -coverprofile tmp-coverage-reports/coverage.out
go tool cover -html=tmp-coverage-reports/coverage.out -o tmp-coverage-reports/coverage.html


prettier . --write

gofmt -w .

go build -o dist/bin/community-administration-cli .