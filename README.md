# Community Administration CLI

see [example](./example/README.md) to use the cli.

## CLI help

```txt
NAME:
   Community Administration CLI - A event sourcing cli for community administration

USAGE:
   Community Administration CLI [global options] command [command options] [arguments...]

VERSION:
   v0.0.4

COMMANDS:
   subscription-renewal  generate markdown for subscription renewals
   help, h               Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --timestamp value  Go back in time or go to the future with timestamp. Leave empty to use current time.
   --help, -h         show help
   --version, -v      print the version
```

## CLI subscription-renewal

```txt
NAME:
   Community Administration CLI subscription-renewal - generate markdown for subscription renewals

USAGE:
   Community Administration CLI subscription-renewal [command options] [arguments...]

OPTIONS:
   --subscription-renewal-year value  Subscription renewal year (default: 0)
   --help, -h                        show help
```

## CLI add-member-event

```txt
NAME:
   Community Administration CLI add-member-event - Add an member event

USAGE:
   Community Administration CLI add-member-event [command options] [arguments...]

OPTIONS:
   --yaml value
   --help, -h    show help
```
