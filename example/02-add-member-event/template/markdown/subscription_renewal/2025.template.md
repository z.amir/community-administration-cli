# Betreft: verlenging lidmaatschap {{ .Community.OrganizationName}}

Datum: {{ .Date.Local.Format "2-1-2006" }}

Aan:\
{{.Person.OrganizationName}}\
T.a.v. {{.Person.Name}}\
{{ .Person.OrganizationAddress.Street }} {{ .Person.OrganizationAddress.Housenumber }} {{ .Person.OrganizationAddress.HousenumberAddition }}\
{{ .Person.OrganizationAddress.PostalCode }} {{ .Person.OrganizationAddress.City }}

---

Beste {{.Person.Name}},

Dank dat je lid bent van {{ .Community.OrganizationName}}. Samen geven we invulling aan de vereniging. Per 1 januari {{.SubscriptionRenewalYear}} wordt jouw lidmaatschap automatisch met een jaar verlengd.

## Contributie

De hoogte van de contributie is gelijk gebleven ten opzichte van 2024. We verzoeken je de contributie € 399,00 euro voor het lidmaatschapsjaar {{.SubscriptionRenewalYear}} (kalenderjaar) over te maken naar rekening {{.Community.Iban}} ten name van {{ .Community.OrganizationName}} te {{ .Community.OrganizationAddress.City }}.

De vereniging is niet btw-plichtig, dus in het contributiebedrag is geen BTW opgenomen.
We verzoeken je om jouw contributie uiterlijk 31 januari {{.SubscriptionRenewalYear}} te voldoen.
Na betaling ontvang je een betalingsbevestiging voor jouw administratie.

Met vriendelijke groet,

Namens {{ .Community.OrganizationName}},

John doe\
Secretaris
