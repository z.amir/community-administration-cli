# {{.Id}} {{.Name}}

{{if not .MembershipActive}}

> **_NOTE:_** DEPRECATED

{{end}}

Lidnummer: {{ .MembershipId }}
Email: <{{ .Email }}>
Telefoonnummer: {{ .Phonenumber }}

## {{ .OrganizationName}}

### Organisatieadres

{{ .OrganizationAddress.Street }} {{ .OrganizationAddress.Housenumber }} {{ .OrganizationAddress.HousenumberAddition }}
{{ .OrganizationAddress.PostalCode }} {{ .OrganizationAddress.City }}

### Correspondentieadres

{{if not .CorrespondenceAddress}}
N.V.T.
{{else}}
{{ .CorrespondenceAddress.Street }} {{ .CorrespondenceAddress.Housenumber }} {{ .CorrespondenceAddress.HousenumberAddition }}
{{ .CorrespondenceAddress.PostalCode }} {{ .CorrespondenceAddress.City }}
{{end}}

## Contributies

| Verenigingsjaar | Status |
| --------------- | ------ |

{{- range .Subscriptions }}
| {{ .Year }} | {{ .Status }} |
{{- end}}

## Event log

| Datum | Type |
| ----- | ---- |

{{- range .Events }}
| {{ .Date.Format "2006-01-02T15:04:05-0700" }} | {{ .Type }} |
{{- end}}

Dit bestand is gegenereed op {{.DateGenerated.Local.Format "02-01-2006"}}, pas deze niet met de hand aan.
