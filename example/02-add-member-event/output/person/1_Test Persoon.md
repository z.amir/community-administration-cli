# 1 Test Persoon

Lidnummer: 1
Email: <test.persoon@community-administration-cli.nl>
Telefoonnummer: +31687654321

## Test bedrijf

### Organisatieadres

Test straat 1
1234AB Test stad

### Correspondentieadres

N.V.T.

## Contributies

| Verenigingsjaar | Status  |
| --------------- | ------- |
| 2022            | BETAALD |
| 2023            | BETAALD |

## Event log

| Datum                    | Type                                |
| ------------------------ | ----------------------------------- |
| 2022-01-01T00:00:00+0100 | PERSON_APPROVED                     |
| 2022-01-02T00:00:00+0100 | CREATE_PAYMENT_REQUEST              |
| 2022-01-03T00:00:00+0100 | APPLICATION_FORM_SENT               |
| 2022-01-04T00:00:00+0100 | SUBSCRIPTION_FORM_RECEIVED          |
| 2022-01-04T00:59:59+0100 | MEMBER_INFO_COMPLETED               |
| 2022-01-04T01:00:00+0100 | PAYMENT_COMPLETED                   |
| 2022-01-04T01:59:59+0100 | SUBSCRIPTION_PAYED                  |
| 2022-01-04T03:59:59+0100 | MEMBERSHIP_ID_ASSIGNED              |
| 2022-01-04T04:58:58+0100 | PAYMENT_CONFIRMATION_SENT           |
| 2022-01-04T04:59:59+0100 | WELCOME_LETTER_SENT                 |
| 2022-06-01T00:00:00+0200 | MEMBER_INFO_CHANGED                 |
| 2022-12-01T00:00:00+0100 | CREATE_PAYMENT_REQUEST              |
| 2022-12-01T00:59:59+0100 | SUBSCRIPTION_RENEWAL_SENT           |
| 2022-12-02T00:00:00+0100 | PAYMENT_COMPLETED                   |
| 2023-12-11T22:00:56+0000 | SUBSCRIPTION_PAYED                  |
| 2023-12-11T22:00:56+0000 | PAYMENT_CONFIRMATION_LETTER_CREATED |

Dit bestand is gegenereed op 12-12-2023, pas deze niet met de hand aan.
