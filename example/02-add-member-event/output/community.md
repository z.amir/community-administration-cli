# Test community

KvK nummer: 1234567
IBAN: GB33BUKB20201555555555

## Organisatieadres

Straatnaam 1
1234AB Stad

## Contributies

| Verenigingsjaar | Bedrag |
| --------------- | ------ |
| 2022            | 100    |
| 2023            | 100    |
| 2024            | 100    |

## Event log

| Datum               | Type                   |
| ------------------- | ---------------------- |
| 01-12-2021 00:00:00 | SUBSCRIPTION_FEE_SET   |
| 01-01-2022 00:00:00 | COMMUNITY_INFO_CHANGED |
| 01-12-2022 00:00:00 | SUBSCRIPTION_FEE_SET   |
| 01-12-2023 00:00:00 | SUBSCRIPTION_FEE_SET   |

Dit bestand is gegenereed op 12-12-2023, pas deze niet met de hand aan.
