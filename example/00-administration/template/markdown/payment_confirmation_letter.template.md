# Betreft: betalingsbevestiging

Datum: {{ .Date.Local.Format "2-1-2006" }}

Aan:\
{{.Person.OrganizationName}}\
T.a.v. {{.Person.Name}}\
{{ .Person.OrganizationAddress.Street }} {{ .Person.OrganizationAddress.Housenumber }} {{ .Person.OrganizationAddress.HousenumberAddition }}\
{{ .Person.OrganizationAddress.PostalCode }} {{ .Person.OrganizationAddress.City }}

---

Beste {{.Person.Name}},

Bij dezen bevestigen wij dat wij uw contributiebetaling in goede orde hebben ontvangen.

> Contributie {{ .Community.OrganizationName}} - verenigingsjaar {{.CommunitySubscription.Year}}\
> Bedrag: € {{ divide .CommunitySubscription.Amount 100}}

De vereniging is niet btw-plichtig, dus in het contributiebedrag is geen BTW opgenomen.

Met vriendelijke groet,

{{.Community.OrganizationName}}\
{{ .Community.OrganizationAddress.Street }} {{ .Community.OrganizationAddress.Housenumber }} {{ .Community.OrganizationAddress.HousenumberAddition }}\
{{ .Community.OrganizationAddress.PostalCode }} {{ .Community.OrganizationAddress.City }}
