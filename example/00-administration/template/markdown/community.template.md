# {{.OrganizationName}}

KvK nummer: {{ .ChamberOfCommerceNumber }}
IBAN: {{ .Iban }}

## Organisatieadres

{{ .OrganizationAddress.Street }} {{ .OrganizationAddress.Housenumber }} {{ .OrganizationAddress.HousenumberAddition }}
{{ .OrganizationAddress.PostalCode }} {{ .OrganizationAddress.City }}

## Contributies

| Verenigingsjaar | Bedrag |
| --------------- | ------ |

{{- range .Subscriptions }}
| {{ .Year }} | {{ .Amount }} |
{{- end}}

## Event log

| Datum | Type |
| ----- | ---- |

{{- range .Events }}
| {{ .Date.Format "2006-01-02T15:04:05-0700" }} | {{ .Type }} |
{{- end}}

Dit bestand is gegenereed op {{.DateGenerated.Local.Format "02-01-2006"}}, pas deze niet met de hand aan.
