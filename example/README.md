# Example

```sh
docker run -it --rm --name community-administration-cli -v ./cli:/cli registry.gitlab.com/z.amir/community-administration-cli

prettier . --write
```

## Markdown to PDF

To convert Markdown to PDF we recommend [pandoc](https://pandoc.org)

```sh
pandoc -s -V geometry:margin=1in adminstration/markdown/letter/subscription_renewal/2024/20231206\ Brief\ verlenging\ lidmaatschap\ Test\ community\ Test\ Persoon.md -o administration/markdown/letter/subscription_renewal/2024/20231206\ Brief\ verlenging\ lidmaatschap\ Test\ community\ Test\ Persoon.pdf
```
